/**
 * 
 */
package test.com.jolao.jms.listener;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jolao.jms.listener.ConsumerListener;

/**
 * @author JoLao
 *
 */
public class ConsumerListenerTest {
	private TextMessage message; 
	private ApplicationContext ctx;
	private ConsumerListener listener;
	private static String json = "{vendorName:\"Microsofttest3\",firstName:\"BobTest3\",lastName:\"SmithTest3\",address:\"123 Main test3\",city:\"TulsaTest3\",state:\"OKTest3\",zip:\"71345Test3\",email:\"Bob@microsoft.test3\",phoneNumber:\"test-123-test3\"}";
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		 ctx = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		 listener = (ConsumerListener) ctx.getBean("consumerListener");
		 message = createMock(TextMessage.class);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		  ((ConfigurableApplicationContext) ctx).close();	
	}

	/**
	 * Test method for {@link com.jolao.jms.listener.ConsumerListener#onMessage(javax.jms.Message)}.
	 * @throws JMSException 
	 */
	@Test
	public void testOnMessage() throws JMSException {
		expect(message.getText()).andReturn(json);
		replay(message);
		listener.onMessage(message);
		verify(message);
	}

}
